/** @format */

import {AppRegistry} from 'react-native';
import App from './src/App';
import {displayName} from './app.json';

AppRegistry.registerComponent(displayName, () => App);
