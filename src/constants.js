import React from 'react'
import { StatusBar, StyleSheet, Dimensions } from 'react-native'

export const COLOR_PRIMARY = '#ffffff'
export const COLOR_ACCENT = '#6295B2'
export const COLOR_ACCENT_2 = '#CCC'
export const COLOR_HEADER = '#f3f3f3'

export const FONT_SIZE = 12
export const FONT_HEADER = FONT_SIZE * 1.5
export const HAIRLINE_WIDTH = StyleSheet.hairlineWidth
export const STATUS_BAR_HEIGHT = StatusBar.currentHeight

export const TOUCHABLE_OPACITY = 0.5

export const BORDER_WIDTH = 1
export const BORDER_AVATAR_WIDTH = BORDER_WIDTH * 2