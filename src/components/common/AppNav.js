import React, { Component } from 'react'
import { StyleSheet,TouchableOpacity, Text, View } from 'react-native'
import Icon from 'react-native-vector-icons/Feather'
import {COLOR_ACCENT} from '../../constants'

class AppNav extends Component {
  constructor(props){
    super(props)
  }
  
  render() {
    const {navigate} = this.props.navigation
    return(
      <View style={styles.navbar}>
        <TouchableOpacity style={styles.icon} onPress={() => navigate('EventsList')} >
          <Icon name="calendar" size={30} color="#fff"/>
          <Text style={styles.iconText}>Events</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.icon} onPress={() => navigate('Sponsors')}>
          <Icon name="heart" size={30} color="#fff" />
          <Text style={styles.iconText}>Sponsors</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.icon} color="#fff" onPress={() => navigate('About')} >
          <Icon name="settings" size={30} color="#fff" />
          <Text style={styles.iconText}>About</Text>
        </TouchableOpacity>
      </View>      
    )
  }
}

const styles = StyleSheet.create({
  navbar: {
      backgroundColor: COLOR_ACCENT,
      position: 'absolute',
      flex:0.1,
      flexDirection:'row',
      left: 0,
      right: 0,
      bottom: -10,
  },
  icon:{
    flex:1,
    padding:10,
    color:'#fff',
    alignItems:'center',
  },
  iconText:{
    color:'#fff',
    textAlign: 'center',
    fontSize: 18,
    marginBottom: 10
  }
});

export default AppNav