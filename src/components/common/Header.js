import React, { Component } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { COLOR_ACCENT, COLOR_PRIMARY,COLOR_HEADER, COLOR_ACCENT_2, FONT_HEADER,BORDER_WIDTH } from '../../constants';

class Header extends Component {
    constructor(props){
        super(props)
    }
    render() {
        return (
            <View style={styles.header}>
                <Text style={styles.headerText}>{this.props.text.toUpperCase()}</Text>
            </View> 
        )
    }
}

const styles = StyleSheet.create({
    header: {
      justifyContent: 'center',
      alignItems: 'center',
      padding: 15,
      borderBottomWidth: BORDER_WIDTH,
      borderBottomColor: COLOR_ACCENT_2,
      backgroundColor: COLOR_HEADER
    },
    headerText: {
        fontSize: FONT_HEADER,
    }
});
  
export default Header