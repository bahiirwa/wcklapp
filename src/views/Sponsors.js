import React, {Component} from 'react'
import {ScrollView,Text, StyleSheet, View} from 'react-native'
import { COLOR_ACCENT, COLOR_PRIMARY } from '../constants'
import Header from '../components/common/Header'
import AppNav from '../components/common/AppNav'

class Sponsors extends Component {
    constructor(props){
      super(props)
    }

    static navigationOptions = {
		header: null
    }

    render(){
        return(
            <View style={STYLES.container}>
                <Header text="Sponsors"></Header>
                <ScrollView style={STYLES.body}>
                    <Text>Sponsor 1</Text>
                    <Text>Sponsor 2</Text>
                    <Text>Sponsor 3</Text>
                </ScrollView>
                <AppNav navigation = {this.props.navigation} />
            </View>
        )
    }
}

const STYLES = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:COLOR_PRIMARY
    },
    body:{
        padding:10,
    },
})

export default Sponsors