import React, {Component} from 'react'
import {ScrollView,StyleSheet, Text, View} from 'react-native'
import AppNav from '../components/common/AppNav'
import Header from '../components/common/Header'
import { COLOR_ACCENT, COLOR_PRIMARY } from '../constants'

class About extends Component {

    static navigationOptions = {
		header: null
  }
    render(){
        return(
            <View style={STYLES.container}>
                <Header text="About"></Header>
                <ScrollView style={STYLES.body}>
                    <Text style={STYLES.heading}>I'm Klavens</Text>
                    <Text>
                       My work is to curate all Tech events in & around Kampala for events enthusiasts to be timely informed(synced)!
                    </Text>
                </ScrollView>

                <AppNav navigation = {this.props.navigation}/>
            </View>
        )
    }
}

const STYLES = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:COLOR_PRIMARY
    },
    body:{
        padding:20
    },
    heading:{
        color:COLOR_ACCENT,
        textAlign:'center',
        fontWeight:'bold',
        fontSize:20,
        marginBottom:20,
    }
})

export default About