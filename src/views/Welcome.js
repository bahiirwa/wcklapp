import React, {Component} from 'react'
import {ActivityIndicator,Text, StyleSheet, View} from 'react-native'
import {displayName} from '../../app.json'

class Welcome extends Component {
    constructor(props){
      super(props)
    }

    static navigationOptions = {
		header: null
    }
    
    render(){
        setTimeout(() => {
            this.props.navigation.navigate('EventsList')
        },1000 )

        return(
            <View style={STYLES.container}>
                <Text style={{color:'#fff',marginBottom:15}}>{displayName}</Text>
                <ActivityIndicator size="small" color="#ffffff" />
            </View>
        )
    }
}

const STYLES = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        textAlign:'center',
        backgroundColor: '#6295B2',
    },
    colors: {
        color: '#fff'
    }
})

export default Welcome