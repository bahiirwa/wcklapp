import React, {Component} from 'react'
import {StyleSheet,View, FlatList,TouchableOpacity } from 'react-native'
import {List,Avatar,Divider, ListItem } from 'react-native-elements'
import AppNav from '../components/common/AppNav'
import Header from '../components/common/Header'
import {data} from '../info.json'
import { COLOR_ACCENT, FONT_SIZE,COLOR_PRIMARY,BORDER_AVATAR_WIDTH } from '../constants'

class EventsList extends Component {
  constructor(props){
    super(props)
  }

  static navigationOptions = {
		header: null
  }

  renderRow ({ item }) {
    return (
      <TouchableOpacity
        onPress= {()=>alert('Element Pressed!')}
      >
        <ListItem
          roundAvatar
          title={item.title}
          chevronColor={COLOR_ACCENT}
          titleStyle={{
            color:COLOR_ACCENT,
          }}
          avatar={<Avatar
            rounded
            large
            title={item.date}
            titleStyle={{
              color:COLOR_ACCENT,
              fontSize:FONT_SIZE
            }}
            containerStyle={{
              borderWidth:BORDER_AVATAR_WIDTH,
              borderStyle:'solid',
              borderRadius:50,
              borderColor:COLOR_ACCENT,
              backgroundColor:COLOR_PRIMARY
            }}
            overlayContainerStyle={{
              backgroundColor: COLOR_PRIMARY
            }}

          />}
        />
      </TouchableOpacity>
    )
  }
  renderSeparator = () => {
    return(
      <View
        style = {{
          height:1,
          backgroundColor:COLOR_ACCENT,
        }}
      />
    )
  }
  render() {
    return (
      <View style={styles.container}>
        <Header text="Events"></Header>
        <List containerStyle={{borderTopWidth:0,borderBottomWidth:0,marginTop:0}}>
          <FlatList
            data={data.events}
            renderItem={this.renderRow}
            keyExtractor={item => item.title}
          />
        </List>
        <AppNav navigation = {this.props.navigation}/>
    </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:COLOR_PRIMARY
  },
  body:{
    padding:10,
    flex:1,
    paddingBottom:15
  },
})

export default EventsList