/**
 * 
 * Open up App
 */
import React, {Component} from 'react';
import About from './views/About'
import EventsList from './views/EventsList'
import Sponsors from './views/Sponsors'
import {createStackNavigator} from 'react-navigation'

class App extends Component {
  render() {
    return(<NAVIGATION />)
  }
}

const NAVIGATION = createStackNavigator({EventsList,Sponsors,About})

export default App